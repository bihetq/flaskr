#					/ Flaskr /


***

![Main Page](https://raw.githubusercontent.com/bihetq/flaskr/master/images/main_page.png) 
***

Flaskr est une application de bloging.

Flaskr est écrit en Python (2.7.9) et utilise le framework [Flask](http://flask.pocoo.org).

La base de données est gérée avec SQLite.

Cette application est le résultat du [tutoriel de Flask](http://flask.pocoo.org/docs/0.10/tutorial).

Le but est d'apprendre à utiliser ce Framework au travers de la réalisation d'un blog écrit en Python.

